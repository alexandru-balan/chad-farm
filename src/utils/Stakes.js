import { LP } from "./LP"

export const stakes = [
    {
        id: 1,
        icon: LP.FARM,
        title: "Profit Sharing",
        deposit: 'FARM',
        earn: 'FARM',
        APY: 346.75,
        links: {
            "0x2555...Fc50": "https://etherscan.io/address/0x25550Cccbd68533Fa04bFD3e3AC4D09f9e00Fc50",
            "0x8f5a...436c": "https://etherscan.io/address/0x8f5adc58b32d4e5ca02eac0e293d35855999436c"
        }
    },
    {
        id: 2,
        icon: LP.UNISWAP,
        title: "Uniswap Farm",
        deposit: 'UNISWAP_LP',
        earn: 'FARM',
        APY: 455.09,
        links: {
            'Uniswap Farm': "https://uniswap.info/pair/0x514906FC121c7878424a5C928cad1852CC545892",
            "0x99b0...f9bf": "https://etherscan.io/address/0x99b0d6641A63Ce173E6EB063b3d3AED9A35Cf9bf"
        }
    },
    {
        id: 3,
        icon: LP.CREATIVITY,
        title: "Creativity Farm",
        deposit: 'Creativity',
        earn: 'FARM',
        APY: undefined,
        rewards: 18000,
        links: {
            "Announcement": "https://www.publish0x.com/harvestfinance/announcing-harvestfinance-creativity-contest-round-2-plus-bo-xnldqpx"
        }
    },
    {
        id: 4,
        icon: require('@/assets/crypto/uni-eth-usdt.svg'),
        title: "Uni ETH-USDT",
        deposit: 'fUNISWAP_LP',
        earn: 'FARM',
        APY: 22.41,
        links: {
            "Uni ETH-USDT": "https://info.uniswap.org/pair/0x0d4a11d5eeaac28ec3f61d100daf4d40471f1852",
            "0x7507...C960": "https://etherscan.io/address/0x75071F2653fBC902EBaff908d4c68712a5d1C960"
        }
    },
    {
        id: 5,
        icon: require('@/assets/crypto/tusd.svg'),
        title: "TUSD Farm",
        deposit: 'fTUSD',
        earn: 'FARM',
        APY: 9.75,
        links: {
            "0xeC56...eCdA": "https://etherscan.io/address/0xeC56a21CF0D7FeB93C25587C12bFfe094aa0eCdA" 
        }
    },
    {
        id: 6,
        icon: require('@/assets/crypto/uni-eth-usdc.svg'),
        title: "Uni ETH-USDC",
        deposit: 'fUNISWAP_LP',
        earn: 'FARM',
        APY: 14.51,
        links: {
            "Uni ETH-USDC": "https://info.uniswap.org/pair/0xb4e16d0168e52d35cacd2c6185b44281ec28c9dc",
            "0x1567...64b5": "https://etherscan.io/address/0x156733b89Ac5C704F3217FEe2949A9D4A73764b5"
        }
    },
    {
        id: 7,
        icon: require('@/assets/crypto/uni-eth-dai.svg'),
        title: "Uni ETH-DAI",
        deposit: 'fUNISWAP_LP',
        earn: 'FARM',
        APY: 14.87,
        links: {
            "Uni ETH-DAI": "https://info.uniswap.org/pair/0xa478c2975ab1ea89e8196811f51a7b7ade33eb11",
            "0x7aeb...859c": "https://etherscan.io/address/0x7aeb36e22e60397098C2a5C51f0A5fB06e7b859c"
        }
    },
    {
        id: 8,
        icon: require('@/assets/crypto/uni-eth-wbtc.svg'),
        title: "Uni ETH-WBTC",
        deposit: 'fUNISWAP_LP',
        earn: 'FARM',
        APY: 15.11,
        links: {
            "Uni ETH-WBTC": "https://info.uniswap.org/pair/0xbb2b8038a1640196fbe3e38816f3e67cba72d940",
            "0xF118...CB93": "https://etherscan.io/address/0xF1181A71CC331958AE2cA2aAD0784Acfc436CB93"
        }
    },
    {
        id: 9,
        icon: require('@/assets/crypto/eth.svg'),
        title: "WETH Farm",
        deposit: 'fWETH',
        earn: 'FARM',
        APY: 5.84,
        links: {
            "0x3DA9...ff8e": "https://etherscan.io/address/0x3DA9D911301f8144bdF5c3c67886e5373DCdff8e"
        }
    },
    {
        id: 10,
        icon: require('@/assets/crypto/sushi-wbtc-tbtc.svg'),
        title: "SUSHI: WBTC-TBTC",
        deposit: 'fSUSHI-WBTC-TBTC',
        earn: 'FARM',
        APY: 11.51,
        links: {
            "SUSHI WBTC-TBTC": "https://exchange.sushiswapclassic.org/#/add/0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599/0x8dAEBADE922dF735c38C80C7eBD708Af50815fAa",
            "0x9523...EF34": "https://etherscan.io/address/0x9523FdC055F503F73FF40D7F66850F409D80EF34"
        }
    },
    {
        id: 11,
        icon: require('@/assets/crypto/usdc.svg'),
        title: "USDC Farm",
        deposit: 'fUSDC',
        earn: 'FARM',
        APY: 9.06,
        links: {
            "0x4F7c...78Bd": "https://etherscan.io/address/0x4F7c28cCb0F1Dbd1388209C67eEc234273C878Bd"
        }
    },
    {
        id: 12,
        icon: require('@/assets/crypto/dai.svg'),
        title: "DAI Farm",
        deposit: 'fDAI',
        earn: 'FARM',
        APY: 7.69,
        links: {
            "0x15d3...5B4A": "https://etherscan.io/address/0x15d3A64B2d5ab9E152F16593Cdebc4bB165B5B4A"
        }
    },
    {
        id: 13,
        icon: require('@/assets/crypto/usdt.svg'),
        title: "USDT Farm",
        deposit: 'fUSDT',
        earn: 'FARM',
        APY: 10.99,
        links: {
            "0x6ac4...94a2": "https://etherscan.io/address/0x6ac4a7ab91e6fd098e13b7d347c6d4d1494994a2"
        }
    },
    {
        id: 14,
        icon: require('@/assets/crypto/wbtc.svg'),
        title: "WBTC Farm",
        deposit: 'fWBTC',
        earn: 'FARM',
        APY: 6.51,
        links: {
            "0x917d...A77B": "https://etherscan.io/address/0x917d6480Ec60cBddd6CbD0C8EA317Bcc709EA77B"
        }
    },
    {
        id: 15,
        icon: require('@/assets/crypto/renbtc.svg'),
        title: "RENBTC Farm",
        deposit: 'fRENBTC',
        earn: 'FARM',
        APY: 6.38,
        links: {
            "0x7b8F...b4F6": "https://etherscan.io/address/0x7b8Ff8884590f44e10Ea8105730fe637Ce0cb4F6"
        }
    },
    {
        id: 16,
        icon: require('@/assets/crypto/renwbtc.svg'),
        title: "CRVRENBTC Farm",
        deposit: 'fCRVRENBTC',
        earn: 'FARM',
        APY: 3.40,
        links: {
            "0xA3Cf...3Db5": "https://etherscan.io/address/0xA3Cf8D1CEe996253FAD1F8e3d68BDCba7B3A3Db5"
        }
    }
]