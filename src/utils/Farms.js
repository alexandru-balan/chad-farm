/** A file to store all info on farms */
import { LP } from "@/utils/LP";

export const farms = [
    {
        cryptoLogo: require('@/assets/crypto/eth.svg'),
        farmTitle: 'WETH',
        farmAPY: 8.57,
        lps: [LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/dai.svg'),
        farmTitle: 'DAI',
        farmAPY: 18.74,
        lps: [LP.CURVE, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/usdc.svg'),
        farmTitle: 'USDC',
        farmAPY: 19.87,
        lps: [LP.CURVE, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/usdt.svg'),
        farmTitle: 'USDT',
        farmAPY: 21.27,
        lps: [LP.CURVE, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/tusd.svg'),
        farmTitle: 'TUSD',
        farmAPY: 31.68,
        lps: [LP.CURVE, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/renbtc.svg'),
        farmTitle: 'RENBTC',
        farmAPY: 17.60,
        lps: [LP.CURVE, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/wbtc.svg'),
        farmTitle: 'WBTC',
        farmAPY: 14.59,
        lps: [LP.CURVE, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/renwbtc.svg'),
        farmTitle: 'CRV:RENWBTC',
        farmAPY: 8.45,
        lps: [LP.CURVE, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/uni-eth-dai.svg'),
        farmTitle: 'UNI:ETH-DAI',
        farmAPY: 34.70,
        lps: [LP.UNISWAP, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/uni-eth-usdc.svg'),
        farmTitle: 'UNI:ETH-USDC',
        farmAPY: 41.91,
        lps: [LP.UNISWAP, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/uni-eth-usdt.svg'),
        farmTitle: 'UNI:ETH-USDT',
        farmAPY: 41.95,
        lps: [LP.UNISWAP, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/uni-eth-wbtc.svg'),
        farmTitle: 'UNI:ETH-WBTC',
        farmAPY: 38.37,
        lps: [LP.UNISWAP, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/sushi-wbtc-tbtc.svg'),
        farmTitle: 'SUSHI:WBTC-TBTC',
        farmAPY: 37.90,
        lps: [LP.SUSHI, LP.FARM]
    },
    {
        cryptoLogo: require('@/assets/crypto/farm.svg'),
        farmTitle: 'FARM',
        farmAPY: 89.25,
        lps: [LP.FARM]
    }
]