/**  A class for enumerating liquidity providers */

export const LP = {
    UNISWAP: require('@/assets/lp/uniswap.png'),
    SUSHI: require('@/assets/lp/sushi.png'),
    FARM: require('@/assets/lp/farm.png'),
    CURVE: require('@/assets/lp/curve.png'),
    CREATIVITY: require('@/assets/lp/palette.svg')
};