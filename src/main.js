import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// Importing the css framework
import UIkit from 'uikit';

// Importing the fontawesome
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { faTractor, faBars, faSun, faMoon, faExternalLinkAlt } from "@fortawesome/free-solid-svg-icons";

library.add(faTractor, faBars, faSun, faMoon, faExternalLinkAlt);

Vue.config.productionTip = false

Vue.component('fa-icon', FontAwesomeIcon);

UIkit;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
