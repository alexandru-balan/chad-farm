import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/stake',
    name: 'Stake',
    component: () => import(/* webpackChunkName: "stake" */ '../views/Stake.vue')
  },
  {
    path: '/stake/:id',
    component: () => import(/* webpackChunkName: "stake" */ '../components/reusables/StakeMenu.vue'),
  },
  {
    path: '/faq',
    name: 'FAQ',
    component: () => import(/* webpackChunkName: "faq" */ '../views/FAQ.vue')
  }
]

const router = new VueRouter({
  routes,
  linkExactActiveClass: "active",
  scrollBehavior: function() {
    return {x: 0, y: 0}
  }
})

export default router
